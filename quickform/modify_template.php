<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Include admin wrapper script
require(LEPTON_PATH."/modules/admin.php");

// include module class
$oQUICKFORM = quickform::getInstance();

// set validation
$oREQUEST = LEPTON_request::getInstance();

// handle on incoming action
$action = "edit";
if ( isset( $_POST["action"] ))
{
	$input_fields = array (
		 'action'			=> array ('type' => 'string_clean', 'default' => "edit")
	);	
	$valid_fields = $oREQUEST->testPostValues($input_fields);
	$action = strtolower( $valid_fields["action"] );
};

// process action
$error = "";
$show = true;

switch( $action )
{
	case "save":
		// validate input
		$input_fields = array (
			 'template_name'	=> array ('type' => 'string_clean', 'default' => "")
			,'template_data'	=> array ('type' => 'string', 'default' => "")
		);
		$valid_fields = $oREQUEST->testPostValues($input_fields);

		// get template name
		$template_name = strtolower( $valid_fields["template_name"] );
		$template_data = $valid_fields["template_data"]??"";

		// do not change quickform default templates as they will be overwritten with next release
		if ( substr ( $template_name, 0 , 3 ) == "qf_" )
		{
			$error = "QUICKFORM_TEMPLATE";
			break;
		}

		// get template file: path . language to use / template name
		$template_path  = __DIR__;
		$template_lang  = $oQUICKFORM->get_page_language( $page_id );
		$template_exist = $oQUICKFORM->get_template( $page_id, $template_path, $template_lang, $template_name );

		// set template content
		if ( true == file_put_contents($template_path . $template_lang . DIRECTORY_SEPARATOR . $template_name, $template_data) )
		{
			// update template name (in case it has been changed)
			$update_when_modified = true; 
			$fields = array( "template" => $template_name );

			$database->build_and_execute(
				"update",
				TABLE_PREFIX."mod_quickform",
				$fields,
				"`section_id` = " . $section_id
			);

			$admin->print_success($TEXT["SUCCESS"], ADMIN_URL."/pages/modify.php?page_id=".(int)$page_id);
		} else { 
			$admin->print_error($TEXT["ERROR"], ADMIN_URL."/pages/modify.php?page_id=".(int)$page_id);
		}
		break;

	default:
		// validate input
		$input_fields = array (
			'template'	=> array ('type' => 'string_clean', 'default' => "", 'range' => "" )
		);
		$valid_fields = $oREQUEST->testPostValues($input_fields);

		// get template file: path . language to use / template name
		$template_path = __DIR__;
		$template_lang = $oQUICKFORM->get_page_language( $page_id );
		$template_name = strtolower( $valid_fields["template"] ); 
		$template_exist = $oQUICKFORM->get_template( $page_id, $template_path, $template_lang, $template_name );

		// get template content
		if ( $template_exist === true )
		{
			$template_data = file_get_contents( $template_path . $template_lang . DIRECTORY_SEPARATOR . $template_name );
		} else {
			$template_data = PHP_EOL . PHP_EOL . "      Template not found !!!";
		}

		// validate template name
		if ( substr ( $template_name, 0 , 3 ) == "qf_" )
		{
			$error = "QUICKFORM_TEMPLATE";
		}
		break;
};

// show backend template
if ( $show == true )
{
	// get language setup
	$MOD_QUICKFORM = $oQUICKFORM->language;

	require_once LEPTON_PATH."/modules/edit_area/class.editorinfo.php";
	$edit_area = edit_area::registerEditArea("code_area", "html");

	// prepare url's used
	$urls = Array(
		  "action"	=> ADMIN_URL."/pages/modify.php?page_id=".$page_id."&section_id=".$section_id
		, "help"	=> "https://doc.lepton-cms.org/docu/english/tutorials/doc-quickform.php"
	);

	// set template values
	$page_values = array(
			 "form_action"		=> $_SERVER["SCRIPT_NAME"]
			,"edit_area"		=> $edit_area
			,"oQUICKFORM"		=> $oQUICKFORM
			,"template_name"	=> $template_name
			,"template_data"	=> $template_data
			,"urls"				=> $urls
			,"leptoken"			=> get_leptoken()
			,"page_id"			=> $page_id
			,"section_id"		=> $section_id
			,"MESSAGE_CLASS"	=> "hidden"
			,"STATUSMESSAGE"	=> ""
		);

	// set error
	if ( $error == "QUICKFORM_TEMPLATE" )
	{
		$page_values["MESSAGE_CLASS"]	= "";
		$page_values["STATUSMESSAGE"]	= $MOD_QUICKFORM[ $error ];
	}

	// show screen
	$oTWIG = lib_twig_box::getInstance();	
	$oTWIG->registerModule( "quickform" );
	echo $oTWIG->render(
		 "@quickform/modify_template.lte"
		,$page_values
	);
}

// Print admin footer
$admin->print_footer();

?>
