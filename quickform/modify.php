<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// get main instance
$admin = LEPTON_admin::getInstance();
$oQUICKFORM = quickform::getInstance();
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule( 'quickform' );

// get settings for section
$settings = $oQUICKFORM->get_settings( $section_id );

 // leptoken
$leptoken = get_leptoken();

// initialize with defaults
$msgid		= 0;
$history	= '';
$anchor		= '';
$grpcur		= 'INBOX';
$grplist  = $oQUICKFORM->get_groups( $section_id );
$rowcur		= 50;
$rowlist	= array(10, 25, 50, 75, 100);
if( isset($settings['usenbritems']))	{ $rowcur  = $settings['usenbritems']; }
$viewcur	= 'TABLE';
$viewlist	= array( "CLASSIC", "TABLE");
if( isset($settings['useview']))		{ $viewcur = $settings['useview']; }

// set validation
$oREQUEST = LEPTON_request::getInstance();

// handle on incoming action
$action = 'show';
if ( isset( $_POST["page_id"] ))
{
	// spamcheck and edittpl goes to different scripts
	// therefore no need to check here
	$buttons = array( 'showmodinfo', 'showgroup', 'hidegroup', 'setrows', 'changeview'
						, 'closegroup', 'movemsg', 'deletemsg');
	
	// add default fields
	$input_fields = array (
		  'grpcur'		=> array ('type' => 'string_clean', 'default' => '')
		, 'rowcur'		=> array ('type' => 'integer+', 'default' => '')
		, 'viewcur'		=> array ('type' => 'string_clean', 'default' => '')
		, 'msgid'		=> array ('type' => 'string_clean', 'default' => '')
		, 'grpnew'		=> array ('type' => 'string_clean', 'default' => '')
		, 'anchor'		=> array ('type' => 'string_clean', 'default' => '')
	);
	// add buttons
	foreach ($buttons as $button)
	{
		$input_fields[ 'btn_' . $button ] = array ('type' => 'string_clean', 'default' => '');
	}
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// check for button action value, must start with button name
	foreach ($buttons as $button)
	{
		if ( SubStr( $valid_fields['btn_' . $button], 0, StrLen( $button )) == $button )
		{
			$action = $button;
			break;
		}
	}
};

// set this section as last edited section, due to it is open in backend directly
if ( ! empty($action))	{ $_SESSION['last_edit_section'] = $section_id; }

// overwrite with input if not initial
if ( $action <> 'show' )
{
	$grpcur  = $valid_fields[ 'grpcur' ];
	$rowcur  = $valid_fields[ 'rowcur' ];
	$viewcur = $valid_fields[ 'viewcur' ];
	$msgid   = $valid_fields[ 'msgid' ];
	$grpnew  = $valid_fields[ 'grpnew' ];
	$anchor  = $valid_fields[ 'anchor' ];
}

// handle on incoming action
switch( $action )
{
	case "showmodinfo":
		$oQUICKFORM->show_info( $page_id, $section_id );
		return true;
		break;

	case "showgroup":
		// + 2 for underscore and at least 1 char for group name
		if ( StrLen( $valid_fields['btn_' . $action] ) >= StrLen( $action ) + 2 )
		{
			$new = SubStr( $valid_fields['btn_' . $action], StrLen( $action ) + 1 );
			if ( array_search($new, array_column($grplist, 'msg_group')) !== False )	{ $grpcur = $new; }
		}

		$history = $oQUICKFORM->get_history( $section_id, $grpcur, $rowcur, $viewcur );
		$anchor = "groups";
		break;
	case "hidegroup":
		// hide the history details
		break;
	case "setrows":
		// + 2 for underscore and at least 1 digit for number of rows
		if ( StrLen( $valid_fields['btn_' . $action] ) >= StrLen( $action ) + 2 )
		{
			$new = (int)SubStr( $valid_fields['btn_' . $action], StrLen( $action ) + 1 );
			if ( in_array($new, $rowlist) )	{ $rowcur = $new; }
		}

		$oQUICKFORM->set_rows( $section_id, $rowcur );
		$history = $oQUICKFORM->get_history( $section_id, $grpcur, $rowcur, $viewcur );
		$anchor = "groups";
		break;
	case "changeview":
		// + 2 for underscore and at least 1 char for view name
		if ( StrLen( $valid_fields['btn_' . $action] ) >= StrLen( $action ) + 2 )
		{
			$new = SubStr( $valid_fields['btn_' . $action], StrLen( $action ) + 1 );
			if ( in_array($new, $viewlist) )	{ $viewcur = $new; }
		}

		$oQUICKFORM->change_view( $section_id, $viewcur );
		$history = $oQUICKFORM->get_history( $section_id, $grpcur, $rowcur, $viewcur );
		$anchor = "groups";
		break;

	case "movemsg":
		$oQUICKFORM->move_msg( $section_id, $msgid, $grpcur, $grpnew, $rowcur );
		$grplist  = $oQUICKFORM->get_groups( $section_id );	// rebuild groups because it may have got a new group while move
		if ( array_search($grpcur, array_column($grplist, 'msg_group')) === False )	{ $grpcur = $grpnew; }	// current group no longer exists, go to new group

		$history = $oQUICKFORM->get_history( $section_id, $grpcur, $rowcur, $viewcur );
		$anchor = $oQUICKFORM->get_anchor_target( $section_id, $msgid, $grpcur );
		break;
	case "deletemsg":
		$oQUICKFORM->delete_msg( $section_id, $msgid, $grpcur, $rowcur );
		$grplist  = $oQUICKFORM->get_groups( $section_id );	// rebuild groups because it may have gone a group while delete last message inside
		if ( array_search($grpcur, array_column($grplist, 'msg_group')) === False )	{ break; }	// current group no longer exists, hide history

		$history = $oQUICKFORM->get_history( $section_id, $grpcur, $rowcur, $viewcur );
		$anchor = $oQUICKFORM->get_anchor_target( $section_id, $msgid, $grpcur );
		break;
} 

/* --------------------
 *	upper part, setting elements
 * -------------------- */
// prepare url's used
$urls = Array(
	  "save"		=> LEPTON_URL."/modules/quickform/save.php"
	, "edittpl"		=> LEPTON_URL.'/modules/quickform/modify_template.php?page_id='.$page_id.'&section_id='.$section_id.'&name='.$settings['template'].'&leptoken='.$leptoken
	, "spamcheck"	=> LEPTON_URL.'/modules/quickform/modify_spamcheck.php?page_id='.$page_id.'&section_id='.$section_id
	, "quickform"	=> ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id
	, "help"		=> "https://doc.lepton-cms.org/docu/english/tutorials/doc-quickform.php"
);

// get language setup
$page_language = $oQUICKFORM->get_page_language( $page_id );
$MOD_QUICKFORM = $oQUICKFORM->language;

if(!isset($settings['email']))   $settings['email']   = SERVER_EMAIL;
if(!isset($settings['subject'])) $settings['subject'] = $MOD_QUICKFORM['SUBJECT'];

// build dropdown list for success page
if (!isset($pages))
{
	$pages = array();
	$oQUICKFORM->build_pagelist(0,$page_id);
}

// Find quickform form templates directory for current language, fallback "en"
$look_up_dir = __DIR__."/templates/".$page_language;
$use_template_dir = is_dir($look_up_dir)
	? $look_up_dir
	: __DIR__."/templates/en"
	;

LEPTON_handle::register( "file_list" );
$templatelist = file_list( 
    $use_template_dir,      // source path
	array(),                // array with sufolders to skip
	false,                  // include hidden files
	"lte",                  // file type pattern
	$use_template_dir."/"   // strip string from files found
);

/* --------------------
 *	prepare and show backend template
 * -------------------- */
// Additional marker settings
$page_values = array(
	 'page_id'				=> $page_id
	,'section_id'			=> $section_id
	,'leptoken'				=> $leptoken
	,'msgid'				=> $msgid
	,'grpcur'				=> $grpcur
	,'rowcur'				=> $rowcur
	,'rowlist'				=> $rowlist
	,'viewcur'				=> $viewcur
	,'urls'					=> $urls
	,'anchor'				=> $anchor
	,'THEME_URL' 			=> THEME_URL
	,'ADMIN_URL' 			=> ADMIN_URL
	,'oQUICKFORM'			=> $oQUICKFORM
	,'settings'				=> $settings
	,'templatelist'			=> $templatelist
	,'pagelist'				=> $pages
	,'grplist'				=> $grplist
	,'history'				=> $history
);

echo $oTWIG->render(
	 "@quickform/modify.lte"
	,$page_values
);

?>
