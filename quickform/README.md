### quickform
============

This module allows you to create a quick and simple form without complicated settings using TWIG template engine.
Quickform is still part of the standard LEPTON package.
Recoded by @pramach for LEPTON 5-series.


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
 

#### Installation

* download latest installation files from [current svn][2]
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon create a page using quickform addon and start to work. <br />
For further details please see [documentation][3].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://gitlab.com/lepton-cms/LEPTON/-/tree/master/upload/modules/quickform
[3]: https://doc.lepton-cms.org/docu/english/tutorials/doc-quickform.php