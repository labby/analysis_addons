{#
/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 #}

{% autoescape false %}
{# --------------------
 # check within LEPTON documentation (via help button) for decription of the possibilities (english only)
 # -------------------- #}
 
{# --------------------
 # set the default field settings
 # might be overwritten by specific field settings
 #   Key		 	= Default	=> Description
 # ----------------------------------------------
 #  *  field		= Null		=> defines the field name & id. Also used as label if label is not specified.
 #  *  label		= Null		=> define special field label. Empty default.
 #  *  value		= Null		=> define a specific value, mainly for single checkboxes. Empty default.
 #  *  type 		= "text"	=> defines the input field type. Default = "text"
 # (*) inputmode	= "vebatim"	=> defines the keyboard layout for mobile devices. Default = alphanumerical keyboard layout
 # (*) placeholder	= Null		=> set a placeholder text to be shown before user enters data. Not transmitted.
 # (*) required		= "r_"		=> mark field as required.
 # (*) autofocus	= Null		=> Set the autofocus on a field. Only 1 per form is allowed. Overruled by first error found.
 # (*) readonly		= Null		=> Set a field to readonly.
 # (*) disabled		= Null		=> Set a field to disabled.
 #
 # (*) such field has additional settings, mainly the attribute string
 # -------------------- #}
{% set initials = { 
  "field": Null
, "label": Null
, "value": Null
, "type": "text"
, "inputmode": "vebatim", "inputmode_attribute": 'inputmode="###"'
, "placeholder": Null, "placeholder_attribute": 'placeholder="###"'
, "required": "r_", "required_attribute": 'required="required"', "required_markup": '<span class="required">*</span>'
, "autofocus": Null, "autofocus_attribute": 'autofocus="autofocus"'
, "readonly": Null, "readonly_attribute": 'readonly="readonly"'
, "disabled": Null, "disabled_attribute": 'disabled="disabled"'
, "class_attribute": 'class="###"'
} %}

{# --------------------
 # define the form
 # -------------------- #}
<div class="quickform">
	<div class="{{ MESSAGE_CLASS }}">{{ STATUSMESSAGE }}</div>
	<div class="{{ FORM_CLASS }}">
		<h2>Contact</h2>
{% if REQUIRED_FIELDS is not empty %}
		<small>Fields marked with {{ required_markup }} are required fields.</small>
{% endif %}

		<form name="form_{{ SECTION_ID }}" id="form_{{ SECTION_ID }}" method="post" action="{{ URL }}" autocomplete="on">
			<input type="hidden" name="header"		value="Contact" />
			<input type="hidden" name="timestamp"	value="{{ TIMESTAMP }}" data-label="Timestamp" />
			<input type="hidden" name="contactid"	value="{{ CONTACT_ID }}" />
			<input type="hidden" name="quickform"	value="{{ SECTION_ID }}" />

			<div class="ui two columns stackable grid container">
				<div class="ui row">

{% set settings = initials|merge({"field": "name", "inputmode": "latin-name", "autofocus": true }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge[{"autofocus": true}] %}
{% elseif IS_ERROR %}{% set settings = settings|merge[{"autofocus": Null}] %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui column">
						<div class="ui form">
							<div class="ui field">
								<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span></label>
								<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
									value="{{ _context[settings.field|upper] }}"
									tabindex="{{tabindex}}"
									{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
									{{ settings.required ? settings.required_attribute }}
									{{ settings.disabled ? settings.disabled_attribute }}
									{{ settings.readonly ? settings.readonly_attribute }}
									{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
									{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
									{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
									data-label="{{ settings.label ?? settings.field|capitalize }}"
								 />
							</div>
						</div>
					</div>

{% set settings = initials|merge({"field": "email", "label": "E-Mail", "type": "email", "inputmode": "email", "placeholder": "e.g. name@domain.tld" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge[{"autofocus": true}] %}
{% elseif IS_ERROR %}{% set settings = settings|merge[{"autofocus": Null}] %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui column">
						<div class="ui form">
							<div class="ui field">
								<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span></label>
								<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
									value="{{ _context[settings.field|upper] }}"
									tabindex="{{tabindex}}"
									{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
									{{ settings.required ? settings.required_attribute }}
									{{ settings.disabled ? settings.disabled_attribute }}
									{{ settings.readonly ? settings.readonly_attribute }}
									{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
									{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
									{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
									data-label="{{ settings.label ?? settings.field|capitalize }}"
								 />
							</div>
						</div>
					</div>
				</div> <!-- end row -->

				<div class="ui row">
{% set settings = initials|merge({"field": "message", "label": "Your message" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge[{"autofocus": true}] %}
{% elseif IS_ERROR %}{% set settings = settings|merge[{"autofocus": Null}] %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui column">
						<div class="ui form">
						  <div class="ui field">
								<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span></label>
								<textarea cols="80" rows="10"
									name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
									tabindex="{{tabindex}}"
									{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
									{{ settings.required ? settings.required_attribute }}
									{{ settings.disabled ? settings.disabled_attribute }}
									{{ settings.readonly ? settings.readonly_attribute }}
									{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
									{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
									{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
									data-label="{{ settings.label ?? settings.field|capitalize }}"
								>{{ _context[settings.field|upper]|default("") }}</textarea>
						  </div>
						</div>
					</div>
				</div> <!-- end row -->

				<div class="ui row">
{% set settings = initials|merge({"field": "dataprotection", "value": "Accepted", "type": "checkbox", "label":"I have read the privacy policy and agree to the processing of my data." }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge[{"autofocus": true}] %}
{% elseif IS_ERROR %}{% set settings = settings|merge[{"autofocus": Null}] %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui column">
						<div class="ui form">
							<div class="ui field">
								<div {{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}>
									<input type="hidden"  value="-"		  name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}" />
									<label>
										<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
											value="{{ settings.value }}"
											tabindex="{{tabindex}}"
											{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
											{{ settings.required ? settings.required_attribute }}
											{{ settings.disabled ? settings.disabled_attribute }}
											{{ settings.readonly ? settings.readonly_attribute }}
											{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
											{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
											{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
											data-label="{{ settings.field|capitalize }}"
											data-value="{{ settings.label }}"
											{% if _context[settings.field|upper] == settings.value %}checked="checked"{% endif %} 
										 />
										 <span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- end row -->

				<div class="spacer1"></div>

				<div class="ui two columns row">
{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui column">
						<button class="ui positive button custom"
								name="Submit"
								type="submit"
								tabindex="{{tabindex}}"
						>Send</button>
					</div>
				</div>
			</div> <!-- end container -->
		</form>
	</div>
</div>
{% endautoescape %}
