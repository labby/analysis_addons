{#
/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 #}

{% autoescape false %}
<!-- Twig template - module: quickform -->
<!-- LepSem -->
<div class="ui basic segment">
	<div class="ui icon red warning message {{ MESSAGE_CLASS }}">{{ STATUSMESSAGE }}</div>

	<form class="ui form" action="{{ form_action }}" method="post">
		<input type="hidden" name="page_id" value="{{ page_id }}" />
		<input type="hidden" name="section_id" value="{{ section_id }}" />
		<input type="hidden" name="leptoken" value="{{ leptoken }}" />
		<input type="hidden" name="action" value="save" />

		<div class="ui two columns stackable grid">
			<div class="ui left aligned column">
				<h2 class="ui header">{{ oQUICKFORM.language.SPAMCHECK }}</h2>
			</div>	
			<div class="ui right aligned column">
				<button class="ui olive basic button" onclick="window.open('{{ urls.help }}','_blank');return false;"><i class="help icon"></i>{{ MENU.HELP }}</button>
			</div>
		</div>

		<div class="ui info message">
			{{ oQUICKFORM.language.SPAM_INTRO }}
		</div>

		<div class="spacer2"></div>

		<div class="field">
			<div class="ui checkbox">	
				<input type="checkbox" name="use_honeypot" id="use_honeypot" {% if data.use_honeypot == 1 %}checked="checked"{% endif %} value="1" />			
				<label>{{ oQUICKFORM.language.USE_HONEYPOT }}</label>
			</div>
		</div>

		<div class="spacer1"></div>
		
		<div class="field">
			<div class="ui checkbox">	
				<input type="checkbox" name="spam_logging" id="spam_logging" {% if data.spam_logging == 1 %}checked="checked"{% endif %}  value="1" />			
				<label>{{ oQUICKFORM.language.SPAM_LOGGING }}</label>
			</div>
		</div>
		
		<div class="spacer2"></div>
		
		<div class="two fields">
			<div class="field">
			  <label>{{ oQUICKFORM.language.SPAM_CHECKTIME }}: </label>
			  <input type="number" required name="spam_checktime" id="spam_checktime"  min="0" max="60" value="{{ data.spam_checktime }}">
			</div>
			<div class="field">
				<label>{{  oQUICKFORM.language.SPAM_FAILPAGE }}: </label>
				<select name="spam_failpage" id="spam_failpage" class="ui fluid dropdown">
					<option value="0" {% if (data.spam_failpage == 0) %}selected='selected'{% endif %}>{{ oQUICKFORM.language.TEXT_FAILMSG }}</option>
					{% for key, page_titel in pagelist %}
						<option value="{{ key }}" {% if(data.spam_failpage==key) %}selected='selected'{% endif %}>{{ page_titel }}</option>
					{% endfor %}
				</select>
			</div>				
		</div>

		<div class="spacer2"></div>
		
		<div class="field">
			<label>{{ oQUICKFORM.language.SPAM_HONEYPOT }}: </label>
			<input type="text" name="spam_honeypot" id="spam_honeypot" value="{{ data.spam_honeypot }}"  />			
		</div>		

		<div class="spacer2"></div>
		
		<div class="ui two column stackable grid">
			<div class="column">
					<button class="right floated positive ui button" type="submit" >{{ TEXT.SAVE }}</button>
			</div>
			<div class="column">
				<button class="negative ui button" type="button" name="cancel" onclick="javascript: window.location = '{{ ADMIN_URL }}/pages/modify.php?page_id={{ page_id }}';">{{ TEXT.CANCEL }}</button>
			</div>
		</div>
	</form>
</div>

{% endautoescape %}
