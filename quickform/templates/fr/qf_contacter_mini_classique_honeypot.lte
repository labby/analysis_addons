{#
/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 #}

{% autoescape false %}
{# --------------------
 # check within LEPTON documentation (via help button) for decription of the possibilities (english only)
 # -------------------- #}
 
{# --------------------
 # define the form
 # -------------------- #}
<div class="quickform">
	<div class="{{ MESSAGE_CLASS }}">{{ STATUSMESSAGE }}</div>
	<div class="{{ FORM_CLASS }}">
		<h2>Contactez</h2>
{% if REQUIRED_FIELDS is not empty %}
		<small>Les champs marqués d'un {{ required_markup }} sont obligatoires.</small>
{% endif %}

		<form name="form_{{ SECTION_ID }}" id="form_{{ SECTION_ID }}" method="post" action="{{ URL }}" autocomplete="on">
			<input type="hidden" name="header"		value="Contactez" />
			<input type="hidden" name="timestamp"	value="{{ TIMESTAMP }}" data-label="Horodatage" />
			<input type="hidden" name="contactid"	value="{{ CONTACT_ID }}" />
			<input type="hidden" name="quickform"	value="{{ SECTION_ID }}" />
{# --- Honeypot id, must be hidden. This is required field when honeypot functionality is used --- #}
			<input type="hidden" name="index"		value="{{ HONEYPOT_INDEX }}" />

			<div class="onethird">
				<label><span>Nom <span class="required">*</span></span>
					<input type="text" name="qf_r_nom" id="nom"
						value="{{ NOM }}"
						tabindex="1"
						required="required"
						class="{{ NAME_ERROR }}"
						inputmode="latin-name"
						data-label="Name"
					 />
				</label>
			</div>

			<div class="twothird pullright">
				<label><span>E-Mail <span class="required">*</span></span>
					<input type="email" name="qf_r_email" id="email"
						value="{{ EMAIL }}"
						tabindex="2"
						placeholder="p.e. name@domain.tld"
						required="required"
						class="{{ EMAIL_ERROR }}"
						inputmode="email"
						data-label="E-Mail"
					 />
				</label>
			</div>

{# --- Honeypot field, should be hidden. Can be placed anywhere in the form --- #}
{% if HONEYPOT_FIELD %}
			<div class="full qf_hp">
				<label><span>{{ HONEYPOT_FIELD }} <span class="required">*</span></span>
					<input type="text" name="qf_{{ HONEYPOT_FIELD|lower }}" id="{{ HONEYPOT_FIELD|lower }}"
						value="{{ _context[HONEYPOT_FIELD|upper] }}"
						tabindex="9"
						placeholder=""
						class="{{ HONEYPOT_FIELD|upper }}_ERROR"
						inputmode="latin-name"
					 />
				</label>
			</div>
{% endif %}

			<div class="full">
				<label><span>Votre message <span class="required">*</span></span>
					<textarea cols="80" rows="10"
						name="qf_r_message" id="message"
						tabindex="3"
						required="required"
						class="{{ MESSAGE_ERROR }}"
						inputmode="vebatim"
						data-label="Votre message"
					>{{ MESSAGE }}</textarea>
				</label>
			</div>

			<div class="full">
				<div class="{{ PROTECTION_DES_DONNEES_ERROR }}">
					<input type="hidden"  value="-"		name="qf_r_protection_des_donnees" id="protection_des_donnees" />
					<label>
						<input type="checkbox"			name="qf_r_protection_des_donnees" id="protection_des_donnees"
							value="Accepté"
							tabindex="4"
							required="required"
							class="{{ PROTECTION_DES_DONNEES_ERROR }}"
							inputmode="vebatim"
							data-label="protection des donnees"
							data-value="Rejeté J'ai lu la politique de confidentialité et j'accepte le traitement de mes données."
							{% if PROTECTION_DES_DONNEES == "Accepté" %}checked="checked"{% endif %} 
						 />
						 <span>Rejeté J'ai lu la politique de confidentialité et j'accepte le traitement de mes données. <span class="required">*</span></span>
					</label>
				</div>
			</div>

			<div class="full">&nbsp;</div>

			<div class="{{ CAPTCHA_CLASS }} full">
				<label><span>protection contre le spam <span class="required">*</span></span>
					<div class="grouping {{ CAPTCHA_ERROR }}">
						{{ CAPTCHA }}
					</div>
				</label>
			</div>

			<div class="onethird">
				<button class="submit"
						name="Submit"
						type="submit"
						tabindex="6"
				>Envoyer</button>
			</div> 
		</form>
	</div>
</div>
{% endautoescape %}
