<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$update_when_modified = true; 
require(LEPTON_PATH.'/modules/admin.php');

if(isset($_POST['section_id']))
{
	$fields = array(
		 'template' 	=> htmlspecialchars(strip_tags($_POST['template'])),
		 'email'		=> htmlspecialchars(strip_tags($_POST['email'])),
		 'subject'		=> htmlspecialchars(strip_tags($_POST['subject'])),
		 'successpage'	=> intval($_POST['successpage'])
	);

	$database->build_and_execute(
		"update",
		TABLE_PREFIX."mod_quickform",
		$fields,
		"`section_id` = ".$section_id
	);
}

// Show success message
$admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);

// Print admin footer
$admin->print_footer();

