<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project, recoded by pramach
 *	@copyright		2012-2022 LEPTON project (based on miniform by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// create the settings table
$table_fields="
	`section_id`			INT				NOT NULL DEFAULT '0',
	`email`					VARCHAR(128)	NOT NULL DEFAULT '',
	`subject`				VARCHAR(128)	NOT NULL DEFAULT '',
	`template`				VARCHAR(64)		NOT NULL DEFAULT '',
	`successpage`			INT				NOT NULL DEFAULT '0',
	`usenbritems`			INT				NOT NULL DEFAULT '50',
	`useview`				VARCHAR(12)		NOT NULL DEFAULT 'CLASSIC',
	`use_honeypot`			tinyint(1)			NULL DEFAULT '0',
	`spam_logging`			tinyint(1)			NULL DEFAULT '0',
	`spam_checktime`		INT					NULL DEFAULT '0',
	`spam_honeypot`			VARCHAR(256)		NULL DEFAULT '',
	`spam_failpage`			INT					NULL DEFAULT '0',
	PRIMARY KEY (`section_id`)	
";
LEPTON_handle::install_table("mod_quickform", $table_fields);


// create the data table
$table_fields="
	`message_id`			INT				NOT NULL auto_increment,
	`section_id`			INT				NOT NULL DEFAULT '0',
	`msg_group`				VARCHAR(32)		NOT NULL DEFAULT 'INBOX',
	`data`					TEXT			NOT NULL,
	`submitted_when`		INT				NOT NULL DEFAULT '0', 
	PRIMARY KEY (`message_id`)
";
LEPTON_handle::install_table("mod_quickform_data", $table_fields);

?>